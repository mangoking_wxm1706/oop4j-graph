# 对象

## 对象基础

### 对象的定义与表示
- [什么是对象](https://www.bilibili.com/video/BV1fe411R75b/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
- [什么是类](https://www.bilibili.com/video/BV1Jc411Z7EZ/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
- [对象与类的内存表示](https://www.bilibili.com/video/BV19g4y1d7Ru/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
- [如何表示共性特征](https://www.bilibili.com/video/BV1Au4y1a7mr/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
### 对象的创建与使用
- [如何创建与引用对象](https://www.bilibili.com/video/BV1bN4y1671H/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
- [如何访问对象](https://www.bilibili.com/video/BV1bN411s7WD/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
- [如何理解静态语境](https://www.bilibili.com/video/BV1fH4y1q7Xf/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)
### 对象间的联系
- [对象间联系类型与表示](https://www.bilibili.com/video/BV1cC4y1S7py/?spm_id_from=333.999.0.0&vd_source=ab9767f8330b9092bf0b35e3238af895)